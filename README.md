# Autoencoding Pixies

This repository accompanies the paper [Autoencoding Pixies: Amortised Variational Inference with Graph Convolutions for Functional Distributional Semantics](https://www.aclweb.org/anthology/2020.acl-main.367/), published at ACL 2020.

## Dependencies

The neural network is implemented using [DyNet](https://github.com/clab/dynet).

Semantic dependency graphs are manipulated using [PyDmrs](https://github.com/delph-in/pydmrs).

## Model architecture

The Pixie Autoencoder is comprised of two parts, a generative model and an inference network,
in the style of a variational autoencoder.

### Generative model

The generative model is implemented in [generate.py](src/generate.py).
Following the framework of Functional Distributional Semantics,
it defines a joint distribution over pixies (entity representations),
truth values of predicates, and observed predicates.

The `WorldModel` class defines a Cardinality Restricted Boltzmann Machine.
The method `neg_energy` implicitly defines
the joint probability distribution over pixies, for any graph topology
(i.e. the edges are labelled with semantic roles, and nodes are unlabelled).

Because exact sampling from a Restricted Boltzmann Machine is intractable,
there are several methods related to sampling.
To impose the cardinality constraint, Belief Propagation is used.
This is a notable drawback of using an energy-based model,
but it allowed a direct comparison with my
[previous work](https://github.com/guyemerson/sem-func) on Functional Distributional Semantics.
An alternative world model (e.g. defining a multivariate Gaussian with precision matrices)
might avoid the need for this added complexity.

The `LexiconModel` class defines a one-layer feedforward network for each predicate,
where the input is a pixie and the output is the probability of truth.
This is given by the method `truth`.
The model also defines a distribution over predicates, given a pixie.
Given all probabilities of truth (for all predicates for the given pixie),
the probability of generating a predicate is proportional to its probability of truth.
By default, the method `truth_and_prob` calculates
both the probability of truth and the probability of being generated.
However, this is computationally expensive, since it requires the entire vocabulary.
The keyword `sample` allows you to approximate the whole vocabulary by a small sample.

The `SemFuncModel` class combines a `WorldModel` with a `LexiconModel`.
All three classes have a `generate` method which produces random samples.

### Inference network

The inference network is implemented in [infer.py](src/infer.py).
It performs approximate Bayesian inference
(finding the distribution over pixies, given observed predicates),
using amortised variational inference.

The `PixieInferenceNetwork` class defines a graph-convolutional network.
It performs approximate Bayesian inference,
so it is defined relative to a semantic function model (such as `SemFuncModel`).
For a world model where pixies are binary feature vectors (as in `WorldModel`),
we can define a distribution over pixies using a probability for each feature.
The method `infer_pixies` takes a semantic dependency graph as input
(each node is labelled with a predicate,
and each edge is labelled with a semantic role),
and outputs a pixie distribution for each node (a vector of probabilities).
Optionally, the keyword `dropout` can be used to apply input dropout,
which means that the predicate is ignored for some nodes.
Three kinds of dropout are supported:
dropping out a given set of nodes,
dropping out exactly one node,
and dropping out each node with some given probability.

The `PixieAutoencoder` class combines a `SemFuncModel` with a `PixieInferenceNetwork`.

## Training

The script [main.py](src/main.py) jointly trains the generative model and the inference network,
on a corpus of semantic dependency graphs.

It first initialises the model using pre-trained vectors.
(This isn't necessary, but it speeds up the early stages of training.
I used sparse PPMI vectors which can be calculated very quickly.)

The ultimate aim is to train the model to maximise
the probability of the observed semantic dependency graphs.
The function `finetune_on_file` iterates through dependency graphs:

For each graph, it uses the inference network
to find the joint pixie distribution conditioned on the observed predicates.
This is a "positive" example for training the world model.
It then uses Belief Propagation to push this joint distribution closer to the prior pixie distribution.
This is a "negative" example for training the world model.
The loss for the world model (`loss`) uses these two examples (averaging over the number of edges),
along with L2 regularisation (technically, L2 outside of an interval).

The lexical model is also trained using the inferred pixies (the "positive" ones).
The loss for the lexical model (`pred_losses`)
uses both the probability of being generated
(this is approximated using a sample of "negative" predicates)
and also the probability of truth
(this improves interpretability of the model).
This is summed over the graph nodes, and L2 regularisation is added
(technically, asymmetric L2).

The inference network is trained to minimise KL-Divergence.
Input dropout is applied, so that the model is sometimes forced to use the context.
The loss for the inference network (`kl_loss`)
uses the expected energy of the inferred pixie distribution,
the entropy of the distribution,
the probability of the predicates (both truth and generation),
and L2 regularisation.
The loss due to the predicates is upweighted,
which counteracts the fact that only a sample of negative predicates was used.
(The upweighting can also be seen as a Beta-VAE.)

The function `pretrain_on_file` only trains the inference network,
and not the world model or lexical model.

The script uses `pretrain_on_file` at the beginning of training
(to align the inference network to the pre-initialised generative model),
and then switches to `finetune_on_file`
(to train both the generative model and the inference network).
(The function names are strictly speaking misnomers,
but are hopefully suggestive of what the functions are doing.)
Training is done with multiple CPU processes,
because the Belief Propagation steps are not vector operations and hence not GPU-friendly.
During training, the model is regularly saved to disk.
