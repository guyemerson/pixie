import dynet as dy
import numpy as np

import generate, utils


class PixieInferenceNetwork(utils.DyNetClass):
    def __init__(self, sem_func_model=None, hidden_dims=None, vocab_size=None, num_semroles=None, pixie_dims=None, pixie_card=None, param_collection=None, init_embed=None):
        if sem_func_model is not None:
            vocab_size = sem_func_model.lexicon.vocab_size
            num_semroles = sem_func_model.world.num_semroles
            pixie_dims = sem_func_model.world.dims
            pixie_card = sem_func_model.world.card
        self.hidden_dims = hidden_dims
        self.vocab_size = vocab_size
        self.num_semroles = num_semroles
        self.pixie_dims = pixie_dims
        self.card = pixie_card
        if param_collection is not None:
            self.params = param_collection
        else:
            self.params = dy.ParameterCollection()
        if init_embed is None:
            self.embed = self.params.add_lookup_parameters((vocab_size, hidden_dims[0]), name='embed')
        else:
            self.embed = self.params.lookup_parameters_from_numpy(init_embed, name='embed')
        self.embed_def = self.params.add_parameters((hidden_dims[0],), name='embeddefault')
        self.embed_def_semrole = self.params.add_parameters((num_semroles, hidden_dims[0]), name='embeddefaultsemrole')
        self.embed_def_invrole = self.params.add_parameters((num_semroles, hidden_dims[0]), name='embeddefaultinvrole')
        self.num_layers = len(hidden_dims)
        all_dims = hidden_dims + [pixie_dims]
        self.w_semrole = [self.params.add_parameters((num_semroles, all_dims[i+1], all_dims[i]),
                                                            name='semrole{}'.format(i),
                                                            init='normal',
                                                            mean=0,
                                                            std=0.4)
                          for i in range(self.num_layers)]
        self.w_invrole = [self.params.add_parameters((num_semroles, all_dims[i+1], all_dims[i]),
                                                            name='invrole{}'.format(i),
                                                            init='normal',
                                                            mean=0,
                                                            std=0.4)
                          for i in range(self.num_layers)]
        self.w_selfloop = [self.params.add_parameters((all_dims[i+1], all_dims[i]),
                                                      name='selfloop{}'.format(i),
                                                      init='normal',
                                                      mean=0,
                                                      std=0.4)
                           for i in range(self.num_layers)]
    
    @property
    def pixie_card(self):
        return dy.scalarInput(self.card)
    
    @property
    def spec(self):
        return {'vocab_size': self.vocab_size,
                'num_semroles': self.num_semroles,
                'pixie_dims': self.pixie_dims,
                'pixie_card': self.card,
                'hidden_dims': self.hidden_dims}
    
    def init_default_pixie(self, graph, index):
        return self.embed_def \
            + sum(self.embed_def_semrole[role]
                  for _, _, role, _ in graph.get_out(index, itr=True)) \
            + sum(self.embed_def_invrole[role]
                  for _, _, role, _ in graph.get_in(index, itr=True))
    
    def init_unmasked(self, graph):
        return [dy.lookup(self.embed, node.pred) for node in graph.nodes]
    
    def init_masked(self, graph, mask):
        return [self.init_default_pixie(graph, i)
                if mask_i
                else dy.lookup(self.embed, graph[i].pred) 
                for i, mask_i in enumerate(mask)]
    
    def init_pixies(self, graph, dropout=None, dropone_prob=1):
        if dropout is None:
            return self.init_unmasked(graph)
        else:
            if dropout is True:
                mask = [node.pred is None for node in graph.nodes]
            elif dropout is 'one':
                if np.random.random() < dropone_prob:
                    mask = [False] * len(graph)
                    mask[np.random.randint(len(graph))] = True
                else:
                    return self.init_unmasked(graph)
            else:
                mask = np.random.binomial(n=1, p=dropout, size=len(graph))
            return self.init_masked(graph, mask)
    
    def convolve_pixies(self, graph, pixies):
        # Tanh layers with convolutional connections
        for r in range(self.num_layers):
            convolved = [self.w_selfloop[r] * x
                         + sum(self.w_semrole[r][role] * pixies[end]
                               for _, end, role, _ in graph.get_out(i, itr=True))
                         + sum(self.w_invrole[r][role] * pixies[start]
                               for start, _, role, _ in graph.get_in(i, itr=True))
                         for i, x in enumerate(pixies)]
            pixies = [dy.tanh(x) for x in convolved]
        # Convert the last tanh to a sigmoid, i.e. rescale to [0,1]
        pixies = [(x + dy.scalarInput(1)) * dy.scalarInput(0.5) for x in pixies]
        # If the total activation is above the cardinality, rescale
        pixies = [x * dy.bmin(dy.scalarInput(1),
                              dy.cdiv(self.pixie_card, dy.sum_elems(x)))
                  for x in pixies]
        return pixies
    
    def infer_pixies(self, graph, **kwargs):
        pixies = self.init_pixies(graph, **kwargs)
        return self.convolve_pixies(graph, pixies)


class PixieAutoencoder(utils.DyNetClass):
    def __init__(self, hidden_dims, *args, param_collection=None, init_embed=None, **kwargs):
        if param_collection is not None:
            self.params = param_collection
        else:
            self.params = dy.ParameterCollection()
        generation_collection = self.params.add_subcollection('generation')
        inference_collection = self.params.add_subcollection('inference')
        self.model = generate.SemFuncModel(*args, param_collection=generation_collection, **kwargs)
        self.encoder = PixieInferenceNetwork(self.model, hidden_dims, param_collection=inference_collection, init_embed=init_embed)
    
    @property
    def spec(self):
        combined_spec = self.model.spec
        combined_spec['hidden_dims'] = self.encoder.hidden_dims
        return combined_spec
    
    def infer_truth(self, graph, **kwargs):
        pixies = self.encoder.infer_pixies(graph, **kwargs)
        return [self.model.lexicon.all_truth(x) for x in pixies]
