import pydmrs.core
import numpy as np

def vso(i,j,k):
    g = pydmrs.core.ListDmrs()
    g.add_node(pydmrs.core.Node(0, i))
    g.add_node(pydmrs.core.Node(1, j))
    g.add_node(pydmrs.core.Node(2, k))
    g.add_link(pydmrs.core.Link(0, 1, 0, None))
    g.add_link(pydmrs.core.Link(0, 2, 1, None))
    return g

def vs(i,j):
    g = pydmrs.core.ListDmrs()
    g.add_node(pydmrs.core.Node(0, i))
    g.add_node(pydmrs.core.Node(1, j))
    g.add_link(pydmrs.core.Link(0, 1, 0, None))
    return g

def vo(i,j):
    g = pydmrs.core.ListDmrs()
    g.add_node(pydmrs.core.Node(0, i))
    g.add_node(pydmrs.core.Node(1, j))
    g.add_link(pydmrs.core.Link(0, 1, 1, None))    
    return g

mini_data = [vso(0,1,2), vso(0,2,3)]

clusters = [[0,1,2],
            [3,4,5],
            [6,7,8],
            [9,10,11],
            [12,13,14],
            [15,16,17],
            [18,19,20]]

data = []
freq = np.zeros(21)

for i in clusters[0]:
    for j in clusters[4]:
        for k in clusters[5]:
            data.append(vso(i,j,k))
            freq[i] += 1
            freq[j] += 1
            freq[k] += 1

for i in clusters[0]:
    for j in clusters[5]:
        for k in clusters[6]:
            data.append(vso(i,j,k))
            freq[i] += 1
            freq[j] += 1
            freq[k] += 1

for i in clusters[1]:
    for j in clusters[4]:
        for k in clusters[5]:
            data.append(vso(i,j,k))
            freq[i] += 1
            freq[j] += 1
            freq[k] += 1

for i in clusters[2]:
    for j in clusters[4]:
            data.append(vs(i,j))
            freq[i] += 1
            freq[j] += 1
    for j in clusters[5]:
            data.append(vo(i,j))
            freq[i] += 1
            freq[j] += 1

for i in clusters[3]:
    for j in clusters[5]:
            data.append(vs(i,j))
            freq[i] += 1
            freq[j] += 1
    for j in clusters[6]:
            data.append(vo(i,j))
            freq[i] += 1
            freq[j] += 1

adv_data = [vso(0,12,18),
            vso(0,15,15),
            vso(0,18,12),
            vso(0,0,0)]

dropout_data = [vso(0,None,15),
                vso(0,12,None),
                vso(0,None,18),
                vso(0,15,None)]

topology = [vso(None,None,None),
            vs(None,None),
            vo(None,None)]

rough_pretrained_w = np.random.uniform(-1, 0, (2,30,30))
rough_pretrained_w[:,:15,15:] = np.random.uniform(0, 1, (2,15,15))

rough_pretrained_v = - np.ones((21,30))
rough_pretrained_v[:12,:15] = np.random.uniform(-1, 3, (12,15))
rough_pretrained_v[12:,15:] = np.random.uniform(-1, 3, (9,15))

rough_pretrained_e = np.random.normal(size=(21,100), scale=0.5)
rough_pretrained_e[:12,50:] = 0
rough_pretrained_e[12:,:50] = 0

fine_pretrained_w = np.random.uniform(-1, 0, (2,30,30))

def pretrain_w_vso(v_clust, s_clust, o_clust, v_ind, s_ind, o_ind):
    fine_pretrained_w[0,v_ind,s_ind] = 2
    fine_pretrained_w[1,v_ind,o_ind] = 2
    for i in v_clust:
        fine_pretrained_w[0,i,s_ind] = 1
        fine_pretrained_w[1,i,o_ind] = 1
        for j in s_clust:
            fine_pretrained_w[0,i,j] = .5
        for j in o_clust:
            fine_pretrained_w[1,i,j] = .5
    for j in s_clust:
        fine_pretrained_w[0,v_ind,j] = 1
    for k in o_clust:
        fine_pretrained_w[1,v_ind,j] = 1

def pretrain_w_vs(v_clust, s_clust, v_ind, s_ind):
    fine_pretrained_w[0,v_ind,s_ind] = 2
    for i in v_clust:
        fine_pretrained_w[0,i,s_ind] = 1
        for j in s_clust:
            fine_pretrained_w[0,i,j] = .5
    for j in s_clust:
        fine_pretrained_w[0,v_ind,j] = 1

def pretrain_w_vo(v_clust, o_clust, v_ind, o_ind):
    fine_pretrained_w[1,v_ind,o_ind] = 2
    for i in v_clust:
        fine_pretrained_w[1,i,o_ind] = 1
        for j in o_clust:
            fine_pretrained_w[1,i,j] = .5
    for j in o_clust:
        fine_pretrained_w[1,v_ind,j] = 1

pretrain_w_vso(clusters[0], clusters[4], clusters[5], 21, 27, 28)
pretrain_w_vso(clusters[0], clusters[5], clusters[6], 22, 28, 29)
pretrain_w_vso(clusters[1], clusters[4], clusters[5], 21, 27, 28)
pretrain_w_vs(clusters[2], clusters[4], 23, 27)
pretrain_w_vo(clusters[2], clusters[5], 24, 28)
pretrain_w_vs(clusters[3], clusters[5], 25, 28)
pretrain_w_vo(clusters[3], clusters[6], 26, 29)

fine_pretrained_v = np.random.uniform(-5, -2, (21,30))
fine_pretrained_v[:12,:12] = -1
fine_pretrained_v[:12,21:27] = -2
fine_pretrained_v[12:,12:21] = -2
fine_pretrained_v[12:,27:] = -1

cluster_feat = [[21,22],
                [21],
                [23,24],
                [25,26],
                [27],
                [28],
                [29]]

for clust, js in enumerate(cluster_feat):
    for i in clusters[clust]:
        fine_pretrained_v[i,i] = 5
        for j in js:
            fine_pretrained_v[i,j] = 5

fine_pretrained_e = np.copy(rough_pretrained_e)
fine_pretrained_e[:12,:12] = np.eye(12)
fine_pretrained_e[12:,50:59] = np.eye(9)