import dynet_config

dynet_config.set(mem='2048')

import dynet as dy
import numpy as np
import random
import infer, dummy_graph
import utils

np.set_printoptions(precision=3, floatmode='fixed', sign=' ', suppress=True)

VOCAB_SIZE = 21
PIXIE_DIMS = 30
PIXIE_CARD = 5
NUM_SEMROLES = 2
HIDDEN_DIMS = [50,50]

a = infer.PixieAutoencoder(pixie_dims=PIXIE_DIMS, pixie_card=PIXIE_CARD, vocab_size=VOCAB_SIZE, num_semroles=NUM_SEMROLES, hidden_dims=HIDDEN_DIMS)
m = a.model
i = a.encoder

L2_WLD = 1e-3
L2_LEX = 1e-4
L2_LXN = 1e-3
L2_INF = 1e-3

EPOCHS = 100
DROPOUT = 'one'
NEG_SAMPLE = 5

opt = dy.AdamTrainer(m.world.params, eps=1e-3)
pred_opt = dy.AdamTrainer(m.lexicon.params, eps=1e-3)
kl_opt = dy.AdamTrainer(i.params, eps=1e-3)

# create a new list, because we will randomise the order
data = [x for x in dummy_graph.data]

def process_graph(g):
    dy.renew_cg()
    
    preds = {node.pred for node in g.nodes}
    
    pd = i.infer_pixies(g, dropout=DROPOUT)
    p = i.infer_pixies(g, dropout=None)
    neg_nobackprop = [dy.inputTensor(x) for x in m.world.update_meanfield(g, [x.npvalue() for x in p])]
    p_nobackprop = [dy.nobackprop(x) for x in p]
    
    loss = dy.cdiv(m.world.neg_energy(g, neg_nobackprop)
                   - m.world.neg_energy(g, p_nobackprop),
                   dy.scalarInput(len(g.links))) \
           + dy.scalarInput(L2_WLD) * utils.L2_norm(m.world.params)
    
    loss.value()
    loss.backward()
    opt.update()
    
    kl_loss = dy.cdiv(- m.world.neg_energy(g, pd, with_backprop=False)
                      + utils.entropy(pd)
                      - sum(dy.log(m.lexicon.prob(node.pred, x, with_backprop=False, sample=NEG_SAMPLE))
                            for x, node in zip(pd, g.nodes)),
                      dy.scalarInput(len(g))) \
              + dy.scalarInput(L2_INF) * utils.L2_norm(i.params, preds=preds)
    
    kl_loss.value()
    kl_loss.backward()
    kl_opt.update()
    
    pred_losses = []
    for x, node in zip(p_nobackprop, g.nodes):
        truth, prob = m.lexicon.truth_and_prob(node.pred, x, sample=NEG_SAMPLE)
        pred_losses.append(-dy.log(truth) + -dy.log(prob))
    pred_loss = dy.cdiv(sum(pred_losses), dy.scalarInput(len(g))) \
                + dy.scalarInput(L2_LEX) * utils.L2_norm(m.lexicon.params, preds=preds) \
                + dy.scalarInput(L2_LXN) * utils.L2_norm_neg_only(m.lexicon.params, preds=preds)
    
    pred_loss.value()
    pred_loss.backward()
    pred_opt.update()
    print('{} {} {}\n'.format(loss.npvalue(), pred_loss.npvalue(), kl_loss.npvalue()), end='')


if __name__ == '__main__':
    for _ in range(EPOCHS):
        random.shuffle(data)
        for g in data:
            process_graph(g)
    
    print()
    final_pixies=[]
    for g in dummy_graph.data:
        dy.renew_cg()
        p = i.infer_pixies(g, dropout=None)
        final_pixies.append([x.npvalue() for x in p])
    print('inferred event pixies')
    for r in [0,27,54]:
        print("graph", r)
        print(final_pixies[r][0])
    
    print()
    c1 = []
    c2 = []
    for k in range(27):
        c1.append(np.max(np.abs(final_pixies[k][0] - final_pixies[k+27][0])))
    for k in range(26):
        c2.append(np.max(np.abs(final_pixies[k][0] - final_pixies[k+1][0])))
    c2.append(np.max(np.abs(final_pixies[26][0] - final_pixies[0][0])))
    print('average distance between event pixies, between clusters 1 and 2')
    print(np.mean(np.array(c1)))
    print('average distance between event pixies, within cluster 1')
    print(np.mean(np.array(c2)))
    
    print()
    print('some randomly generated outputs')
    for j, g in enumerate(dummy_graph.topology):
        print('topology', j)
        for _ in range(3):
            preds = m.generate(g, repeat=50)
            print(preds)

    print()
    print('inferred truth of predicates, given graph')
    print('graph 0')
    for t in a.infer_truth(dummy_graph.data[0], dropout=None):
        print(t.npvalue())
    
    print('graph 0, dropout subject')
    for t in a.infer_truth(dummy_graph.dropout_data[0], dropout=True):
        print(t.npvalue())
    
    print('implausible arguments')
    for t in a.infer_truth(dummy_graph.adv_data[0], dropout=None):
        print(t.npvalue())
    
    print()
    print('neg-energy for some training graphs')
    for r in [0,40,80]:
        g = dummy_graph.data[r]
        p = i.infer_pixies(g, dropout=None)
        print('graph', r)
        print(m.world.neg_energy(g, p).npvalue())
    
    print('neg-energy for some adversarial graphs')
    for r, g in enumerate(dummy_graph.adv_data):
        p = i.infer_pixies(g, dropout=None)
        print('adversarial graph', r)
        print(m.world.neg_energy(g, p).npvalue())
