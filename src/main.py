import dynet_config

dynet_config.set(shared_parameters=True, mem='2048')

import dynet as dy
import numpy as np
import random, pickle, multiprocessing, os.path, gzip
import infer, utils

np.set_printoptions(precision=3, floatmode='fixed', sign=' ', suppress=True)

# Files

INIT_DIR = ''
INIT_NAME = ''
FREQ_FILE = ''
DATA_DIR = ''
OUTPUT_DIR = ''

# Pretrain hyperparameters

VOCAB_SIZE = 87862
PIXIE_DIMS = 800
PIXIE_CARD = 30
NUM_SEMROLES = 2
HIDDEN_DIMS = [300,300]

LINK_REWEIGHT = 1

PRE_L2_INF = 1e-4

PRE_DROPOUT = 'one'
PRE_NEG_SAMPLE = 20

PRE_PRED_LOSS_UPWEIGHT = 100

PRE_EPS = 1e-3
PRE_ALPHA = 1e-3
PRE_ALPHA_DECAY = 0.98

PRE_WORKERS = 16

PRETRAIN_NAME = INIT_NAME + ''

NUM_PRETRAIN_FILES = 3000

# Finetune hyperparameters

DROPOUT = 'one'
DROPONE_PROB = 0.75
NEG_SAMPLE = 32

L2_WLD = 1e-2
WLD_POS_OFFSET = -0.1
WLD_NEG_OFFSET = -0.2
L2_LEX = 1e-7
L2_LXN = 1e-4
L2_INF = 1e-4

PRED_LOSS_UPWEIGHT = 20

EPS = 1e-3

ALPHA_WLD = 10**-5.5
ALPHA_LEX = 10**-3
ALPHA_INF = 10**-4
ALPHA_DECAY = 0.98

WORKERS = 6

FINETUNE_NAME = ''

# Initialise

print(PRETRAIN_NAME)
print(FINETUNE_NAME)
print('loading pretrained weights')

V_FILE = os.path.join(INIT_DIR, '{}-v.pkl.gz'.format(INIT_NAME))
W_FILE = os.path.join(INIT_DIR, '{}-w.pkl.gz'.format(INIT_NAME))
E_FILE = os.path.join(INIT_DIR, 'e.pkl.gz'.format(INIT_NAME))
with gzip.open(V_FILE, 'rb') as f:
    init_v = pickle.load(f)
with gzip.open(W_FILE, 'rb') as f:
    init_w = pickle.load(f) * LINK_REWEIGHT
with gzip.open(E_FILE, 'rb') as f:
    init_e = pickle.load(f)

print('setting up model')

with open(FREQ_FILE, 'rb') as f:
    freq = pickle.load(f)

a = infer.PixieAutoencoder(pixie_dims=PIXIE_DIMS,
                           pixie_card=PIXIE_CARD,
                           vocab_size=VOCAB_SIZE,
                           num_semroles=NUM_SEMROLES,
                           hidden_dims=HIDDEN_DIMS,
                           frequency=freq,
                           init_w=init_w,
                           init_v=init_v,
                           init_embed=init_e)
m = a.model
i = a.encoder

# Define training

pre_kl_opt = dy.AdamTrainer(i.params, eps=PRE_EPS, alpha=PRE_ALPHA)

def pretrain_on_file(fname):
    print('start {}\n'.format(fname), end='', flush=True)
    with open(os.path.join(DATA_DIR, fname), 'rb') as f:
        graphs = pickle.load(f)
    random.shuffle(graphs)
    for g in graphs:
        dy.renew_cg()
        
        pd = i.infer_pixies(g, dropout=PRE_DROPOUT)
        
        kl_loss = - m.world.neg_energy(g, pd, with_backprop=False) \
                  + utils.entropy(pd) \
                  - dy.scalarInput(PRE_PRED_LOSS_UPWEIGHT) * \
                    sum(sum(dy.log(p)
                            for p in m.lexicon.truth_and_prob(node.pred, x, with_backprop=False, sample=PRE_NEG_SAMPLE))
                        for x, node in zip(pd, g.nodes)) \
                  + dy.scalarInput(PRE_L2_INF) * utils.L2_norm(i.params, preds=[])
        
        kl_loss.value()
        kl_loss.backward()
        i.embed.scale_gradient(0)
        pre_kl_opt.update()
        
    print('end {}\n'.format(fname), end='', flush=True)
    pre_kl_opt.set_learning_rate(pre_kl_opt.learning_rate * PRE_ALPHA_DECAY)  

opt = dy.AdamTrainer(m.world.params, eps=EPS, alpha=ALPHA_WLD)
pred_opt = dy.AdamTrainer(m.lexicon.params, eps=EPS, alpha=ALPHA_LEX)
kl_opt = dy.AdamTrainer(i.params, eps=EPS, alpha=ALPHA_INF)

def finetune_on_file(fname):
    print('start {}\n'.format(fname), end='', flush=True)
    with open(os.path.join(DATA_DIR, fname), 'rb') as f:
        graphs = pickle.load(f)
    random.shuffle(graphs)
    for g in graphs:
        dy.renew_cg()
        
        preds = {node.pred for node in g.nodes}
        
        pd = i.infer_pixies(g, dropout=DROPOUT, dropone_prob=DROPONE_PROB)
        p = i.infer_pixies(g, dropout=None)
        neg_nobackprop = [dy.inputTensor(x) for x in m.world.update_meanfield(g, [x.npvalue() for x in p])]
        p_nobackprop = [dy.nobackprop(x) for x in p]
        
        loss = dy.cdiv(m.world.neg_energy(g, neg_nobackprop)
                       - m.world.neg_energy(g, p_nobackprop),
                       dy.scalarInput(len(g.links))) \
               + dy.scalarInput(L2_WLD) * utils.L2_norm_pos_only(m.world.params, offset=WLD_POS_OFFSET) \
               + dy.scalarInput(L2_WLD) * utils.L2_norm_neg_only(m.world.params, offset=WLD_NEG_OFFSET)
        
        loss.value()
        loss.backward()
        opt.update()
        
        kl_loss = dy.cdiv(- m.world.neg_energy(g, pd, with_backprop=False)
                          + utils.entropy(pd)
                          - dy.scalarInput(PRED_LOSS_UPWEIGHT) * sum(sum(dy.log(p)
                                    for p in m.lexicon.truth_and_prob(node.pred, x, with_backprop=False, sample=NEG_SAMPLE))
                                for x, node in zip(pd, g.nodes)),
                          dy.scalarInput(len(g))) \
                  + dy.scalarInput(L2_INF) * utils.L2_norm(i.params, preds=preds)
        
        kl_loss.value()
        kl_loss.backward()
        kl_opt.update()
        
        pred_losses = []
        for x, node in zip(p_nobackprop, g.nodes):
            truth, prob = m.lexicon.truth_and_prob(node.pred, x, sample=NEG_SAMPLE)
            pred_losses.append(-dy.log(truth) + -dy.log(prob))
        pred_loss = dy.cdiv(sum(pred_losses), dy.scalarInput(len(g))) \
                    + dy.scalarInput(L2_LEX) * utils.L2_norm(m.lexicon.params, preds=preds) \
                    + dy.scalarInput(L2_LXN) * utils.L2_norm_neg_only(m.lexicon.params, preds=preds)
        
        pred_loss.value()
        pred_loss.backward()
        pred_opt.update()
        
        #print(m.world.w.npvalue().max(), m.world.w.npvalue().min(), loss.npvalue(), kl_loss.npvalue(), pred_loss.npvalue())
    
    print('end {}\n'.format(fname), end='', flush=True)
    opt.set_learning_rate(opt.learning_rate * ALPHA_DECAY)
    pred_opt.set_learning_rate(pred_opt.learning_rate * ALPHA_DECAY)
    kl_opt.set_learning_rate(kl_opt.learning_rate * ALPHA_DECAY)

# Train

if __name__ == '__main__':
    print('starting training')
    
    files = os.listdir(DATA_DIR)
    random.shuffle(files)
    pretrain_files = files[:NUM_PRETRAIN_FILES]
    finetune_files = files[NUM_PRETRAIN_FILES:]
    
    with multiprocessing.Pool(PRE_WORKERS) as p:
        result = p.map_async(pretrain_on_file, pretrain_files, error_callback=print)
        
        hours = 0
        while not result.ready():
            fname = '{}-{}'.format(PRETRAIN_NAME, hours)
            dy.save(os.path.join(OUTPUT_DIR, fname), [a])
            result.wait(7200)
            hours += 1
        
        fname = '{}-{}'.format(PRETRAIN_NAME, 'final')
        dy.save(os.path.join(OUTPUT_DIR, fname), [a])
    
    full_name = fname + FINETUNE_NAME
    
    with multiprocessing.Pool(WORKERS) as p:
        result = p.map_async(finetune_on_file, finetune_files, error_callback=print)
        
        hours = 0
        while not result.ready():
            fname = '{}-{}'.format(full_name, hours)
            dy.save(os.path.join(OUTPUT_DIR, fname), [a])
            result.wait(7200)
            hours += 1
        
        fname = '{}-{}'.format(full_name, 'final')
        dy.save(os.path.join(OUTPUT_DIR, fname), [a])
