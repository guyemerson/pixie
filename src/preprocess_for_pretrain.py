import numpy as np
import pickle, gzip, os.path
import gensim

PRED_FILE = '/anfs/bigdisc/gete2/wikiwoods/simplevec_all/multicore-5-400-0-1-0-0-8.pkl.gz'
LINK_FILE = '/anfs/bigdisc/gete2/wikiwoods/meanfield_link/multicore-5-400-0-1-0-0-8-1_0-40-0_01-1_0-4_0-0_5-0_2.pkl.gz'
VEC_FILE = '/anfs/bigdisc/gete2/wikiwoods/word2vec/model'
VOCAB_FILE = '/anfs/bigdisc/gete2/wikiwoods/multicore-5-vocab.pkl'
FREQ_FILE = '/anfs/bigdisc/gete2/wikiwoods/multicore-5-freq.pkl'

OUTPUT_DIR = '/anfs/bigdisc/gete2/wikiwoods/init-autoencoder'

PIXIE_CARD = 30
EMBED_DIMS = 300
assert EMBED_DIMS % 3 == 0
VEC_DIMS = EMBED_DIMS // 3

print('loading vocab')

with open(VOCAB_FILE, 'rb') as f:
    vocab = pickle.load(f)
with open(FREQ_FILE, 'rb') as f:
    freq = pickle.load(f)

VOCAB_SIZE = len(vocab)

print('loading embeddings')

wv = gensim.models.Word2Vec.load(VEC_FILE)
init_e = np.zeros((VOCAB_SIZE, EMBED_DIMS))
for i, pred in enumerate(vocab):
    _, pos, _ = pred.rsplit('_', maxsplit=2)
    vec = wv.wv.get_vector(pred)
    init_e[i,VEC_DIMS:2*VEC_DIMS] = vec/2
    if pos == 'v':
        init_e[i,:VEC_DIMS] = vec
    else:
        init_e[i,2*VEC_DIMS:] = vec

print('loading semfuncs')

with gzip.open(PRED_FILE, 'rb') as f:
    init_v = pickle.load(f)

PIXIE_DIMS = init_v.shape[1]

verbs = np.array([x.split('_')[-2] == 'v' for x in vocab])
nouns = np.invert(verbs)
nounverb_freq = np.array(freq, dtype='float')  # to allow division
nounverb_freq[verbs] /= freq[verbs].sum()
nounverb_freq[nouns] /= freq[nouns].sum()
bias = init_v.sum(1) * 2*PIXIE_CARD/PIXIE_DIMS \
       + np.log(1 / nounverb_freq / (1 + 0.01 * nounverb_freq ** -1) - 1)
init_v -= (bias / PIXIE_CARD).reshape((VOCAB_SIZE, 1))

print('loading link weights')

with gzip.open(LINK_FILE, 'rb') as f:
    init_w = pickle.load(f)
init_w -= init_w.mean() * 4

print('saving')

with gzip.open(os.path.join(OUTPUT_DIR, 'v.pkl.gz'), 'wb') as f:
    pickle.dump(init_v, f)

with gzip.open(os.path.join(OUTPUT_DIR, 'w.pkl.gz'), 'wb') as f:
    pickle.dump(init_w, f)

with gzip.open(os.path.join(OUTPUT_DIR, 'e.pkl.gz'), 'wb') as f:
    pickle.dump(init_e, f)

