import dynet as dy
import numpy as np
from scipy.special import expit as sp_expit
from warnings import warn

import utils
    

class WorldModel(utils.DyNetClass):
    def __init__(self, pixie_dims, pixie_card, num_semroles, param_collection=None, init_w=None):
        if param_collection is not None:
            self.params = param_collection

        else:
            self.params = dy.ParameterCollection()
        
        if init_w is None:
            self.w = self.params.add_parameters((num_semroles, pixie_dims, pixie_dims),
                                                name='w',
                                                init='normal',
                                                mean=0,
                                                std=0.1)
        else:
            self.w = self.params.parameters_from_numpy(init_w, name='w')
        #self.b = self.params.add_parameters((pixie_dims,), name='b')
        self.card = pixie_card
        self.dims = pixie_dims
        self.num_semroles = num_semroles 
    
    @property
    def pixie_card(self):
        return dy.scalarInput(self.card)
    
    @property
    def pixie_dims(self):
        return dy.scalarInput(self.dims)
    
    @property
    def spec(self):
        return {'pixie_dims': self.dims,
                'pixie_card': self.card,
                'num_semroles': self.num_semroles}
    
    def neg_energy(self, graph, pixies, with_backprop=True):
        if with_backprop is True:
            w = self.w
        else:
            w = dy.nobackprop(self.w)
        return sum(dy.dot_product(pixies[start],
                                  w[role] * pixies[end])
                   for start, end, role, _ in graph.links)
    """
    def neg_energy(self, graph, pixies):
        return sum(dy.dot_product(p, self.b) for p in pixies) \
            + sum(dy.dot_product(pixies[start],
                                 self.w[role] * pixies[end])
                  for start, end, role, _ in graph.links)
    """
    
    def resample_background(self, graph, pixies):
        w = self.w.npvalue()
        for i in graph:
            neg_energy = sum(w[role] @ pixies[end]
                             for _, end, role, _ in graph.get_out(i)) \
                       + sum(pixies[start] @ w[role]
                             for start, _, role, _ in graph.get_in(i))
            prob = sp_expit(neg_energy)
            # Warning! If neg_energy is above 710, expit returns nan
            # Warning! If neg_energy is above e.g. 15 for 50 units, we may get underflow
            pixies[i] = self.sample_card_restr(prob)
        return pixies
    
    def update_meanfield(self, graph, pixies):
        w = self.w.npvalue()
        for i in graph:
            neg_energy = sum(w[role] @ pixies[end]
                             for _, end, role, _ in graph.get_out(i)) \
                       + sum(pixies[start] @ w[role]
                             for start, _, role, _ in graph.get_in(i))
            prob = sp_expit(neg_energy)
            # Warning! If neg_energy is above 710, expit returns nan
            # Warning! If neg_energy is above e.g. 15 for 50 units, we may get underflow
            pixies[i] = self.meanfield_card_restr(prob)
        return pixies
    
    def update_meanfield_dynet(self, graph, pixies):
        # Shallow copy of the list to avoid side effects
        p = [x for x in pixies]
        # Note that this update is dependent on the order of the nodes
        for i in graph:
            neg_energy = sum(self.w[role] * p[end]
                             for _, end, role, _ in graph.get_out(i)) \
                       + sum(dy.transpose(self.w[role]) * p[start]
                             for start, _, role, _ in graph.get_in(i))
            prob = dy.logistic(neg_energy)
            # Warning! If neg_energy is above 710, expit returns nan
            # Warning! If neg_energy is above e.g. 15 for 50 units, we may get underflow
            p[i] = self.meanfield_card_restr_dynet(prob)
        return p
    
    def sample_card_restr(self, prob):
        """
        Sample a vector from component probabilities,
        restricting the total cardinality.
        :param prob: the probability of each component being on
        """
        # If components are definitely on or off, the cardinality constraint may break:
#         maxed_out = (prob == 1)
#         n_max = maxed_out.sum()
#         if n_max > self.card:
#             warn("{} units with prob 1!".format(n_max))
#             # In this case, just pick units at random from these
#             inds = np.random.choice(np.flatnonzero(maxed_out), size=self.card, replace=False)
#             vec = np.zeros(self._pixie_dims)
#             vec[inds] = 1
#             return vec
        # Sparsity constraints can be enforced using belief propagation (sum-product algorithm)
        # We introduce intermediate nodes which count how many components have been turned on so far
        # Pass messages up
        messages = self.pass_messages_up(prob)
        # Pass messages down
        return self.pass_messages_down(prob, messages)
    
    def meanfield_card_restr(self, prob):
        messages = self.pass_messages_up(prob)
        return self.pass_meanfield_messages_down(prob, messages)
    
    def meanfield_card_restr_dynet(self, prob):
        messages = self.pass_messages_up_dynet(prob)
        return self.pass_meanfield_messages_down_dynet(prob, messages)
    
    def pass_messages_up(self, prob):
        """
        Pass messages for belief propagation from individual components to the cardinality potential
        :param prob: non-sparse probabilities of each component
        :return: cardinality probabilities for successive subsets of components
        """
        intermed = np.zeros((self.dims-1, self.card+1))
        # intermed[i,j] is the probability, ignoring the cardinality potential,
        # that the units from 0 to i have total cardinality j 
        intermed[0,0] = 1-prob[0]
        intermed[0,1] = prob[0]
        # Note: cannot vectorise, because intermed[i] depends on intermed[i-1]
        for i in range(1,self.dims-1):
            intermed[i] = intermed[i-1] * (1 - prob[i])  # unit i is off
            intermed[i,1:] += intermed[i-1,:-1] * prob[i]  # unit i is on

        return intermed
    
    def pass_messages_up_dynet(self, prob):
        """
        Pass messages for belief propagation from individual components to the cardinality potential
        :param prob: non-sparse probabilities of each component
        :return: cardinality probabilities for successive subsets of components
        """
        # intermed[i,j] is the probability, ignoring the cardinality potential,
        # that the units from 0 to i have total cardinality j
        intermed = [dy.concatenate([dy.scalarInput(1) - prob[0],
                                    prob[0],
                                    dy.zeros(self.card-1)])]
        for i in range(1, self.dims-1):
            p = prob[i]
            q = dy.scalarInput(1) - p
            off_prob = dy.cmult(q, intermed[-1])
            on_prob = dy.cmult(p, intermed[-1][:self.card])
            total = off_prob + dy.concatenate([dy.scalarInput(0), on_prob])
            intermed.append(total)
        return intermed
    
    def pass_messages_down(self, prob, intermed):
        """
        Sample a vector, using the cardinality probabilities
        :param prob: non-sparse probabilities of each component
        :param intermed: cardinality probabilities for successive subsets of components
        :return: a sampled vector
        """
        # Fix total number of components, and pass messages down
        vec = np.empty(self.dims, dtype=np.bool_)  # Output vector
        aux = self.card  # Number of components still to come
        # Iteratively sample
#        try:
        for i in range(self.dims-1, -1, -1):  # [D-1, D-2, ..., 0] 
            if aux == i+1:  # All remaining components are on
                vec[:i+1] = 1  # [0, ..., i]
                break
            elif aux == 0:  # All remaining components are off
                vec[:i+1] = 0
                break
            else:
                # Unnormalised probabilities of being on or off:
                p = prob[i]
                ein = p * intermed[i-1][aux-1]
                aus = (1-p) * intermed[i-1][aux]
#                     if ein == 0 and aus == 0:
#                         raise Exception('div zero!')  # TODO make this cleaner
                # Probability of being on:
                on = ein/(ein+aus)
                # Random sample:
                if np.random.binomial(1, on):
                    # Update vector and count
                    vec[i] = 1
                    aux -= 1
                else:
                    # Update vector
                    vec[i] = 0
#         except Exception as e:  # If too many components have high probs (giving underflow errors), just take the highest
#             if e.args[0] != 'div zero!':
#                 raise e
#             warn('div zero!')
#             vec[:] = 0
#             vec[prob.argpartition(-self.C)[-self.C:]] = 1
        return vec
    
    def pass_meanfield_messages_down(self, prob, intermed):
        """
        Update a meanfield vector, using the cardinality probabilities
        :param prob: non-sparse probabilities of each component
        :param intermed: cardinality probabilities for successive subsets of components
        :return: updated meanfield probabilities
        """
        # Fix total number of components, and pass messages down
        vec = np.empty(self.dims)  # Output vector
        # Probability of number of components still to come
        aux = np.zeros(self.card+1)
        aux[self.card] = 1
        # Iteratively update
        for i in range(self.dims-1, 0, -1):  # [D-1, D-2, ..., 1]
            # Unnormalised probabilities of being on or off,
            # for each number of components still to come:
            p = prob[i]
            ein = p * intermed[i-1][:-1]
            aus = (1-p) * intermed[i-1][1:]
            # Probability of being on,
            # for each number of components still to come:
            # (nan_to_num to ignore the undefined values
            #  of aux[j] for j>i)
            on = np.nan_to_num(ein/(ein+aus))
            # Update
            vec[i] = np.dot(on, aux[1:])
            new_aux = np.empty(self.card+1)
            new_aux[0] = aux[0]
            new_aux[1:] = (1-on) * aux[1:]
            new_aux[:-1] += on * aux[1:]
            aux = new_aux
        vec[0] = aux[1]
        return vec
    
    def pass_meanfield_messages_down_dynet(self, prob, intermed):
        """
        Update a meanfield vector, using the cardinality probabilities
        :param prob: non-sparse probabilities of each component
        :param intermed: cardinality probabilities for successive subsets of components
        :return: updated meanfield probabilities
        """
        # Fix total number of components, and pass messages down
        rev_vec = []
        # Probability of number of components still to come
        aux = dy.concatenate([dy.zeros(self.card), dy.scalarInput(1)])
        # Iteratively update
        for i in range(self.dims-1, 0, -1):  # [D-1, D-2, ..., 1]
            # Unnormalised probabilities of being on or off,
            # for each number of components still to come:
            p = prob[i]
            q = dy.scalarInput(1) - p
            ein = dy.cmult(p, intermed[i-1][:self.card])
            aus = dy.cmult(q, intermed[i-1][1:])
            # Probability of being on,
            # for each number of components still to come:
            # (if statement to ignore the undefined values
            #  of aux[j] for j>i)
            if i < self.card-1:
                ein = ein[:i+1]
                aus = aus[:i+1]
                aux = aux[:i+2]
            on = dy.cmult(ein, ein+aus)
            off = dy.scalarInput(1) - on
            # Update
            rev_vec.append(dy.dot_product(on, aux[1:]))
            new_aux_on = dy.cmult(on, aux[1:])
            new_aux_off = dy.cmult(off, aux[1:])
            new_aux = dy.concatenate([new_aux_on, dy.scalarInput(1)]) \
                    + dy.concatenate([aux[0], new_aux_off])
            aux = new_aux
        rev_vec.append(aux[1])
        return dy.concatenate(rev_vec[::-1])
    
    def generate(self, graph, init_pixies=None, repeat=20):
        if init_pixies is None:
            pixies = np.random.binomial(n=1, p=self.card/self.dims, size=(len(graph), self.dims))
        else:
            pixies = init_pixies
        for _ in range(repeat):
            pixies = self.resample_background(graph, pixies)
        return pixies


class LexiconModel():
    def __init__(self, pixie_dims, vocab_size, frequency=None, param_collection=None, init_v=None):
        if param_collection is not None:
            self.params = param_collection
        else:
            self.params = dy.ParameterCollection()
        
        self.vocab_size = vocab_size
        
        if init_v is None:
            self.v = self.params.add_lookup_parameters((vocab_size, pixie_dims),
                                                       name='v',
                                                       init='normal',
                                                       mean=0,
                                                       std=2)
        else:
            self.v = self.params.lookup_parameters_from_numpy(init_v, name='v')
        #self.a = self.params.add_lookup_parameters((vocab_size,), name='a')
        
        if frequency is not None:
            self.freq = frequency
        else:
            self.freq = np.ones(self.vocab_size)
        self.alias_U, self.alias_K = utils.init_alias(self.freq)
        
        self.dims = pixie_dims
        
        self._piby8 = np.pi / 8
    
    @property
    def piby8(self):
        return dy.scalarInput(self._piby8)
    
    @property
    def frequency(self):
        return dy.inputTensor(self.freq)
    
    def __len__(self):
        return self.vocab_size
    
    def param_collection(self):
        return self.params
    
    @property
    def spec(self):
        return {'pixie_dims': self.dims,
                'frequency': self.freq,
                'vocab_size': self.vocab_size}
    
    @staticmethod
    def from_spec(spec, param_collection):
        return LexiconModel(param_collection=param_collection, **spec)
    
    def sample_preds(self, n):
        return set(utils.alias_sample(self.alias_U, self.alias_K, n))
    
    def truth(self, pred, pixie, with_var=True, with_backprop=True):
        if with_backprop is True:
            v = dy.lookup(self.v, pred)
        else:
            v = dy.nobackprop(dy.lookup(self.v, pred))
        neg_energy = dy.dot_product(v, pixie)
        if with_var:
            var = dy.dot_product(dy.square(v),
                                 dy.cmult(pixie, dy.scalarInput(1) - pixie))
            scale = dy.cdiv(dy.scalarInput(1), dy.sqrt(dy.scalarInput(1) + self.piby8 * var))
            neg_energy *= scale
        return dy.logistic(neg_energy)
    
    def truths(self, preds, pixie, **kwargs):
        return dy.concatenate([self.truth(i, pixie, **kwargs) for i in preds])
    
    def all_truth(self, pixie, **kwargs):
        return dy.concatenate([self.truth(i, pixie, **kwargs) for i in range(len(self))])
    
    def truth_and_prob(self, pred, pixie, sample=False, **kwargs):
        if sample is False:
            truths = self.all_truth(pixie, **kwargs)
            return truths[pred], dy.cdiv(truths[pred], dy.sum_elems(truths))
        else:
            other_preds = self.sample_preds(sample)
            other_preds.discard(pred)
            preds = [pred] + list(other_preds)
            truths = self.truths(preds, pixie, **kwargs)
            return truths[0], dy.cdiv(truths[0], dy.sum_elems(truths))
    
    def truth_and_prob_freq(self, pred, pixie, **kwargs):
        truths = self.all_truth(pixie, **kwargs)
        scaled = dy.cmult(truths, self.frequency)
        return truths[pred], dy.cdiv(scaled[pred], dy.sum_elems(scaled))
    
    def prob(self, pred, pixie, **kwargs):
        return self.truth_and_prob(pred, pixie, **kwargs)[1]
    
    def all_prob(self, pixie, **kwargs):
        truths = self.all_truth(pixie, **kwargs)
        return dy.cdiv(truths, dy.sum_elems(truths))
    
    def all_prob_freq(self, pixie, **kwargs):
        truths = self.all_truth(pixie, **kwargs)
        scaled = dy.cmult(truths, self.frequency)
        return dy.cdiv(scaled, dy.sum_elems(scaled))
    
    def generate(self, pixie, **kwargs):
        probs = self.all_prob(dy.inputTensor(pixie), **kwargs).npvalue()
        probs = probs / probs.sum()  # numpy more accurate than dynet?
        return np.random.choice(len(self), p=probs)


class SemFuncModel(utils.DyNetClass):
    def __init__(self, pixie_dims, pixie_card, vocab_size, num_semroles, frequency=None, param_collection=None, init_w=None, init_v=None):
        if param_collection is not None:
            self.params = param_collection
        else:
            self.params = dy.ParameterCollection()
        world_collection = self.params.add_subcollection('world')
        lexicon_collection = self.params.add_subcollection('lexicon')
        self.world = WorldModel(pixie_dims, pixie_card, num_semroles, param_collection=world_collection, init_w=init_w)
        self.lexicon = LexiconModel(pixie_dims, vocab_size, param_collection=lexicon_collection, frequency=frequency, init_v=init_v)
    
    @property
    def spec(self):
        return {'pixie_dims': self.world.dims,
                'pixie_card': self.world.card,
                'num_semroles': self.world.num_semroles,
                'frequency': self.lexicon.freq,
                'vocab_size': self.lexicon.vocab_size}
    
    def generate(self, graph, init_pixies=None, repeat=20, **kwargs):
        pixies = self.world.generate(graph, init_pixies=init_pixies, repeat=repeat)
        preds = [self.lexicon.generate(x, **kwargs) for x in pixies]
        return preds
