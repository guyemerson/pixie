import dynet as dy
import numpy as np

EPS = 0.00001

def entropy(probs):
    q = [dy.scalarInput(1) - x for x in probs]
    return sum(dy.dot_product(x, dy.log(dy.scalarInput(EPS) + x)) for x in probs) \
         + sum(dy.dot_product(x, dy.log(dy.scalarInput(EPS) + x)) for x in q)

def L2_norm(param_collection, preds=None):
    if preds is None:
        return sum(dy.sum_elems(dy.square(param)) for param in param_collection.parameters_list()) \
             + sum(dy.sum_elems(dy.square(param)) for param in param_collection.lookup_parameters_list())
    else:
        return sum(dy.sum_elems(dy.square(param)) for param in param_collection.parameters_list()) \
             + sum(sum(dy.sum_elems(dy.square(dy.lookup(param, pred))) for pred in preds)
                   for param in param_collection.lookup_parameters_list())

def L2_norm_neg_only(param_collection, preds=None, offset=None):
    if preds is not None:
        return sum(dy.sum_elems(dy.square(dy.rectify(-param))) for param in param_collection.parameters_list()) \
             + sum(sum(dy.sum_elems(dy.square(dy.rectify(-dy.lookup(param, pred)))) for pred in preds)
                   for param in param_collection.lookup_parameters_list())
    elif offset is not None:
        return sum(dy.sum_elems(dy.square(dy.rectify(-param + dy.scalarInput(offset)))) for param in param_collection.parameters_list()) \
             + sum(dy.sum_elems(dy.square(dy.rectify(-param + dy.scalarInput(offset)))) for param in param_collection.lookup_parameters_list())
    else:
        return sum(dy.sum_elems(dy.square(dy.rectify(-param))) for param in param_collection.parameters_list()) \
             + sum(dy.sum_elems(dy.square(dy.rectify(-param))) for param in param_collection.lookup_parameters_list())

def L2_norm_pos_only(param_collection, preds=None, offset=None):
    if preds is None:
        return sum(dy.sum_elems(dy.square(dy.rectify(param))) for param in param_collection.parameters_list()) \
             + sum(dy.sum_elems(dy.square(dy.rectify(param))) for param in param_collection.lookup_parameters_list())
    elif offset is not None:
        return sum(dy.sum_elems(dy.square(dy.rectify(param + dy.scalarInput(offset)))) for param in param_collection.parameters_list()) \
             + sum(dy.sum_elems(dy.square(dy.rectify(param + dy.scalarInput(offset)))) for param in param_collection.lookup_parameters_list())
    else:
        return sum(dy.sum_elems(dy.square(dy.rectify(param))) for param in param_collection.parameters_list()) \
             + sum(sum(dy.sum_elems(dy.square(dy.rectify(dy.lookup(param, pred)))) for pred in preds)
                   for param in param_collection.lookup_parameters_list())

def init_alias(prob):
    """
    Initialise arrays for sampling with the alias method
    :param prob: probability array
    :return: probability table, alias table
    """
    N = prob.size
    # Initialise tables
    U = prob.astype('float64') / prob.sum() * N
    K = np.arange(N)
    # Initialise lists with weight above and below 1
    below = [i for i,x in enumerate(U) if x<1]
    above = [i for i,x in enumerate(U) if x>=1]
    # Fill tables
    # In each iteration, we remove one index from the pair of lists
    while above and below:
        # Take a pair of indices, one above and one below
        i = below.pop()
        j = above.pop()
        # Fill in the tables
        K[i] = j
        # Calculate the remaining weight of j, and put it back in the correct list
        U[j] -= (1 - U[i])
        if U[j] < 1:
            below.append(j)
        else:
            above.append(j)
    # Note the final index will have U=1, up to rounding error
    return U, K

def alias_sample_one(U, K):
    """
    Sample from a categorical distribution, using the alias method
    :param U: probability table
    :param K: alias table
    :return: sample
    """
    # Choose a random index
    i = np.random.randint(U.size)
    # Return the index, or the alias
    if np.random.rand() > U[i]:
        return K[i]
    else:
        return i

def alias_sample(U, K, n=None):
    """
    Sample from a categorical distribution, using the alias method
    :param U: probability table
    :param K: alias table
    :param n: number of samples to draw (int or tuple of ints)
    :return: array of samples
    """
    if n:
        # Choose random indices
        i = np.random.randint(U.size, size=n)
        # Choose whether to return indices or aliases
        switch = (np.random.random(n) > U[i])
        return switch * K[i] + np.invert(switch) * i
    else:
        return alias_sample_one(U, K)

class DyNetClass():
    def param_collection(self):
        return self.params
    
    @classmethod
    def from_spec(cls, spec, param_collection):
        return cls(param_collection=param_collection, **spec)
    
    @property
    def spec(self):
        raise NotImplementedError

def cosine(u,v):
    """
    Calculate the cosine similarity between two vectors
    :param u: numpy array
    :param v: numpy array
    :return: similarity
    """
    return np.dot(u,v) / np.sqrt(np.dot(u,u) * np.dot(v,v))
