import dynet as dy
import numpy as np

class SituationParticleCollection():
    def __init__(self, world_model, index_to_graph, num_particles_per_graph=5):
        self.graph = index_to_graph
        self.world = world_model
        self.num = num_particles_per_graph
        self.particles = {i:[np.zeros((len(graph),self.world.dims), dtype=np.bool_) for _ in range(num_particles_per_graph)]
                          for i, graph in self.graph.items()}
        self.next = {i:0 for i in self.graph}
        for i in self.graph:
            for _ in range(self.num):
                self.sample(i)
    
    def sample(self, i):
        # resample the next particle with this graph topology
        self.world.resample_background(self.graph[i], self.particles[i][self.next[i]])
        # update the cyclic counter
        if self.next[i] < self.num - 1:
            self.next[i] += 1
        else:
            self.next[i] = 0
        return self.particles[i][self.next[i]]
    
    def sample_input(self, i):
        return dy.inputTensor(self.sample(i))

class SituationMeanfieldParticleCollection():
    def __init__(self, world_model, index_to_graph, num_particles_per_graph=5):
        self.graph = index_to_graph
        self.world = world_model
        self.num = num_particles_per_graph
        self.particles = {i:[np.random.uniform(size=(len(graph),self.world.dims)) for _ in range(num_particles_per_graph)]
                          for i, graph in self.graph.items()}
        self.next = {i:0 for i in self.graph}
        for i in self.graph:
            for _ in range(self.num):
                self.sample(i)
    
    def sample(self, i):
        # resample the next particle with this graph topology
        self.world.update_meanfield(self.graph[i], self.particles[i][self.next[i]])
        # update the cyclic counter
        if self.next[i] < self.num - 1:
            self.next[i] += 1
        else:
            self.next[i] = 0
        return self.particles[i][self.next[i]]
    
    def sample_input(self, i):
        return dy.inputTensor(self.sample(i))