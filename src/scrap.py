import dynet_config

dynet_config.set(mem="2048", autobatch=False)

import dynet as dy
import numpy as np
import random
from time import time
import generate, infer, dummy_graph
import utils

np.set_printoptions(precision=3, floatmode='fixed', sign=' ', suppress=True)

VOCAB_SIZE = 80000
PIXIE_DIMS = 800
PIXIE_CARD = 30
NUM_SEMROLES = 2
HIDDEN_DIMS = [200,200]

m = generate.SemFuncModel(pixie_dims=PIXIE_DIMS, pixie_card=PIXIE_CARD, vocab_size=VOCAB_SIZE, num_semroles=NUM_SEMROLES, frequency=dummy_graph.freq)
i = infer.PixieInferenceModel(m, hidden_dims=HIDDEN_DIMS)

L2_WLD = 10e-5
L2_LEX = 10e-10
L2_LXN = 10e-5
L2_INF = 10e-5

EPOCHS = 100
DROPOUT = 0.6
BATCH_SIZE = 1
NEG_SAMPLE = 10

opt = dy.AdamTrainer(m.world.params)
pred_opt = dy.AdamTrainer(m.lexicon.params)
kl_opt = dy.AdamTrainer(i.params)

data = [x for x in dummy_graph.data]

t1 = time()
for _ in range(EPOCHS):
    random.shuffle(data)
    for batch in range(0,len(data),BATCH_SIZE):
        dy.renew_cg()
        all_loss = []
        all_kl_loss = []
        all_pred_loss = []
        preds = set()
        for g in data[batch:batch+BATCH_SIZE]:
            pd = i.infer_pixies(g, dropout=DROPOUT)
            
            p = i.infer_pixies(g, dropout=None)
            neg_nobackprop = [dy.inputTensor(x) for x in m.world.update_meanfield(g, [x.npvalue() for x in p])]
            #neg = m.world.update_meanfield_dynet(g, p)
            #neg_nobackprop = [dy.nobackprop(x) for x in neg]
            p_nobackprop = [dy.nobackprop(x) for x in p]
            
            loss = m.world.neg_energy(g, neg_nobackprop) \
                   - m.world.neg_energy(g, p_nobackprop)
            
            all_loss.append(loss)
            
            kl_loss = - m.world.neg_energy(g, pd, with_backprop=False) \
                      + utils.entropy(pd) \
                      - sum(dy.log(m.lexicon.prob(node.pred, x, with_backprop=False, sample=NEG_SAMPLE))
                            for x, node in zip(pd, g.nodes))
            
            all_kl_loss.append(kl_loss)
            
            pred_losses = []
            for x, node in zip(p_nobackprop, g.nodes):
                truth, prob = m.lexicon.truth_and_prob(node.pred, x, sample=NEG_SAMPLE)
                pred_losses.append(-dy.log(truth) + -dy.log(prob))
            pred_loss = sum(pred_losses)
            all_pred_loss.append(pred_loss)
            
            for node in g.nodes:
                preds.add(node.pred)
        
        total_loss = dy.cdiv(sum(all_loss), dy.scalarInput(BATCH_SIZE)) \
                     + dy.scalarInput(L2_WLD) * utils.L2_norm(m.world.params)
        total_loss.value()
        total_loss.backward()
        opt.update()
        total_kl_loss = dy.cdiv(sum(all_kl_loss), dy.scalarInput(BATCH_SIZE)) \
                        + dy.scalarInput(L2_INF) * utils.L2_norm(i.params, preds=preds)
        total_kl_loss.value()
        total_kl_loss.backward()
        kl_opt.update()
        total_pred_loss = dy.cdiv(sum(all_pred_loss), dy.scalarInput(BATCH_SIZE)) \
                          + dy.scalarInput(L2_LEX) * utils.L2_norm(m.lexicon.params, preds=preds) \
                          + dy.scalarInput(L2_LXN) * utils.L2_norm_neg_only(m.lexicon.params, preds=preds)
        total_pred_loss.value()
        total_pred_loss.backward()
        pred_opt.update()

t2 = time()
print(t2-t1)
