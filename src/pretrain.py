import dynet_config

dynet_config.set(shared_parameters=True, mem='2048')

import dynet as dy
import numpy as np
import random, pickle, multiprocessing, os.path, gzip, time
import infer, utils

np.set_printoptions(precision=3, floatmode='fixed', sign=' ', suppress=True)

VOCAB_SIZE = 87862
PIXIE_DIMS = 800
PIXIE_CARD = 30
NUM_SEMROLES = 2
HIDDEN_DIMS = [300,300]

L2_INF = 1e-7

DROPOUT = 0.3
NEG_SAMPLE = 20

PRED_LOSS_UPWEIGHT = 100

WORKERS = 32

V_FILE = '/anfs/bigdisc/gete2/wikiwoods/init-autoencoder/v.pkl.gz'
W_FILE = '/anfs/bigdisc/gete2/wikiwoods/init-autoencoder/w.pkl.gz'
E_FILE = '/anfs/bigdisc/gete2/wikiwoods/init-autoencoder/e.pkl.gz'

FREQ_FILE = '/anfs/bigdisc/gete2/wikiwoods/multicore-5-freq.pkl'

DATA_DIR = '/anfs/bigdisc/gete2/wikiwoods/nocopcore-5-graphs'

OUTPUT_DIR = '/anfs/bigdisc/gete2/wikiwoods/autoencoder'

print('loading pretrained weights')

with gzip.open(V_FILE, 'rb') as f:
    init_v = pickle.load(f)
with gzip.open(W_FILE, 'rb') as f:
    init_w = pickle.load(f)
with gzip.open(E_FILE, 'rb') as f:
    init_e = pickle.load(f)

print('setting up model')

with open(FREQ_FILE, 'rb') as f:
    freq = pickle.load(f)

a = infer.PixieAutoencoder(pixie_dims=PIXIE_DIMS,
                           pixie_card=PIXIE_CARD,
                           vocab_size=VOCAB_SIZE,
                           num_semroles=NUM_SEMROLES,
                           hidden_dims=HIDDEN_DIMS,
                           frequency=freq,
                           init_w=init_w,
                           init_v=init_v,
                           init_embed=init_e)
m = a.model
i = a.encoder
kl_opt = dy.AdamTrainer(i.params, eps=1e-3)


def process_file(fname):
    print('start {}\n'.format(fname), end='', flush=True)
    with open(os.path.join(DATA_DIR, fname), 'rb') as f:
        graphs = pickle.load(f)
    random.shuffle(graphs)
    for g in graphs:
        dy.renew_cg()
        
        pd = i.infer_pixies(g, dropout=DROPOUT)
        """
        n_nodes = dy.scalarInput(len(g))
        n_links = dy.scalarInput(len(g.links))
        
        world_loss = dy.cdiv(- m.world.neg_energy(g, pd, with_backprop=False), n_links)
        entropy_loss = dy.cdiv(utils.entropy(pd), n_nodes)
        pred_loss = dy.cdiv(- sum(sum(dy.log(p)
                                      for p in m.lexicon.truth_and_prob(node.pred, x, with_backprop=False, sample=NEG_SAMPLE))
                                  for x, node in zip(pd, g.nodes)),
                            n_nodes)
        pred_loss *= dy.scalarInput(PRED_LOSS_UPWEIGHT)
        l2_loss = dy.scalarInput(L2_INF) * utils.L2_norm(i.params, preds=[])
        
        kl_loss = world_loss + entropy_loss + pred_loss + l2_loss
        """
        kl_loss = - m.world.neg_energy(g, pd, with_backprop=False) \
                  + utils.entropy(pd) \
                  - dy.scalarInput(PRED_LOSS_UPWEIGHT) * \
                    sum(sum(dy.log(p)
                            for p in m.lexicon.truth_and_prob(node.pred, x, with_backprop=False, sample=NEG_SAMPLE))
                        for x, node in zip(pd, g.nodes)) \
                  + dy.scalarInput(L2_INF) * utils.L2_norm(i.params, preds=[])
        
        kl_loss.value()
        kl_loss.backward()
        i.embed.scale_gradient(0)
        kl_opt.update()
        """
        if len(g) == 3:
            max_diff = np.array([np.abs(pd[1].npvalue() - pd[2].npvalue()).max()])
            print('{} {} {} {} {}\n'.format(world_loss.npvalue(),
                                            entropy_loss.npvalue(),
                                            pred_loss.npvalue(),
                                            l2_loss.npvalue(),
                                            max_diff),
                  end='')
        else:
            print('{} {} {} {}\n'.format(world_loss.npvalue(),
                                         entropy_loss.npvalue(),
                                         pred_loss.npvalue(),
                                         l2_loss.npvalue()),
                  end='')
        """
    print('end {}\n'.format(fname), end='', flush=True)


if __name__ == '__main__':
    print('starting training')
    
    with multiprocessing.Pool(WORKERS) as p:
        result = p.map_async(process_file, os.listdir(DATA_DIR), error_callback=print)
        
        hours = 0
        while not result.ready():
            dy.save(os.path.join(OUTPUT_DIR, str(hours)), [a])
            result.wait(3600)
            hours += 1
        
        dy.save(os.path.join(OUTPUT_DIR, 'final'), [a])
