import os, gzip, pickle
from multiprocessing import Pool
from nltk.stem import WordNetLemmatizer

from pydmrs.components import RealPred
from pydmrs.core import ListDmrs as Dmrs

# Pickled list of predicate names
VOCAB_FILE = '...'
# Directory of gzipped files with DMRS graphs in XML format
INPUT_DIR = '...'
# Directory to save output files with DMRS graphs in pydmrs format
OUTPUT_DIR = '...'
LOG_FILE = 'preprocess_dmrs.log'

WORKERS = 3

print('loading lemmatizer')
lemmatizer = WordNetLemmatizer()

def lemmatize_pred(pred):
    """
    Lemmatize nouns and verbs using WordNet's Morphy
    (currently just nouns and verbs)
    """
    wordform, tag = pred.lemma.rsplit('/', 1)
    pos = tag[0].lower()  # PTB tagset
    if pos in 'nv':  # In principle, could also be 'a' (adjective) or 'r' (adverb)
        lemma = lemmatizer.lemmatize(wordform, pos)
        return RealPred(lemma, pos, 'unknown')
    else:
        return pred

print('loading vocab')
with open(VOCAB_FILE, 'rb') as f:
    vocab = pickle.load(f)

def is_verb(pred, ignore_modal=True, ignore_copula=True, filter_vocab=True):
    if isinstance(pred, RealPred) and pred.pos == 'v':
        if ignore_modal and pred.sense == 'modal':
            return False
        elif ignore_copula and pred.lemma == 'be' and pred.sense == 'id':
            return False
        elif filter_vocab and str(pred) not in vocab:
            return False
        else:
            return True
    else:
        return False

def is_noun(pred, filter_vocab=True):
    if isinstance(pred, RealPred) and pred.pos == 'n':
        if filter_vocab and str(pred) not in vocab:
            return False
        else:
            return True
    else:
        return False

def find_sit(dmrs, node):
    """
    Find if a node represents a situation
    :param dmrs: a Dmrs object
    :param node: a Node object
    :return: a Dmrs object (with 2 or 3 nodes), or None
    """
    # Only consider verbal nodes
    if not is_verb(node.pred):
        return None
    sit = Dmrs(nodes=[node])
    # Look for ARG1 and/or ARG2 nouns
    for role in ('ARG1','ARG2'):
        links = dmrs.get_out(node.nodeid, role)
        if len(links) == 0:
            continue
        elif len(links) == 1:
            link = links.pop()
        else:
            #print('Too many {} links!\n{}\n{}'.format(role,node.pred,' '.join(str(dmrs[link.end].pred) for link in links)))
            continue
        new_node = dmrs[link.end]
        if not is_noun(new_node.pred):
            continue
        if new_node.nodeid in sit:
            print('Repeated node!')
            return None
        sit.add_node(new_node)
        sit.add_link(link)
    # Check if an argument was found
    if len(sit) > 1:
        return sit
    else:
        return None

def extract(dmrs):
    """
    Extract situations from a DMRS in XML form
    :param dmrs: a Dmrs object
    :return: a lit of Dmrs objects
    """
    # Lemmatise OOV items
    for n in dmrs.iter_nodes():
        if isinstance(n.pred, RealPred) and n.pred.pos == 'u':
            n.pred = lemmatize_pred(n.pred)
    # Look for situations
    sits = []
    for n in dmrs.iter_nodes():
        situation = find_sit(dmrs, n)
        if situation is not None:
            sits.append(situation)
    return sits

def extract_file(filename):
    "Extract all situations from a file"
    newname = os.path.splitext(filename)[0]+'.pkl'
    if os.path.exists(os.path.join(OUTPUT_DIR, newname)):
        print('skipping '+filename)
        return
    print(filename)
    with gzip.open(os.path.join(INPUT_DIR, filename),'rb') as f:
        situations = []  # List of situations
        # Each xml will span multiple lines
        xml = b''
        for line in f:
            # Keep adding new lines until we have a whole dmrs
            if line == b'\n':  # Skip blank lines
                continue
            elif line == b'</dmrs>\n':
                xml += line
                try:
                    dmrs = Dmrs.loads_xml(xml)
                except Exception as e:
                    print("Error!")
                    with open(LOG_FILE, 'a') as f:
                        f.write(filename + ':\n' + xml.decode() + '\n' + str(e) + '\n\n')
                    xml = b''
                    continue
                situations.extend(extract(dmrs))
                # Reset the xml string
                xml = b''
            else:
                xml += line
    # Save the situations
    with open(os.path.join(OUTPUT_DIR, newname), 'wb') as f:
        pickle.dump(situations, f)

# Create output directory if necessary
if not os.path.exists(OUTPUT_DIR):
    os.mkdir(OUTPUT_DIR)

# Process each file
all_files = sorted(os.listdir(INPUT_DIR))
with Pool(WORKERS) as p:
    p.map(extract_file, all_files)
